/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode() {}
 *     ListNode(int val) { this.val = val; }
 *     ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class Solution {
    public ListNode reverseKGroup(ListNode head, int k) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        
        ListNode prevGroupTail = dummy;
        
        while (true) {
            ListNode currentGroupHead = prevGroupTail.next;
            ListNode current = currentGroupHead;
            ListNode next = null;
            ListNode prev = null;
            int count = 0;
            
            // Check if there are k nodes left
            ListNode temp = current;
            while (count < k && temp != null) {
                temp = temp.next;
                count++;
            }
            
            // If less than k nodes left, break
            if (count < k)
                break;
            
            // Reverse current group of k nodes
            count = 0;
            while (count < k && current != null) {
                next = current.next;
                current.next = prev;
                prev = current;
                current = next;
                count++;
            }
            
            // Connect the reversed group with the previous group
            prevGroupTail.next = prev;
            currentGroupHead.next = current;
            prevGroupTail = currentGroupHead;
        }
        
        return dummy.next;
    }
}
