//{ Driver Code Starts
import java.util.*;
import java.lang.*;

class Node 
{
    int data;
    Node next;
    Node(int key)
    {
        data = key;
        next = null;
    }
}

class SortLL
{
    static Node head;
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        
        while(t-- > 0) 
        {
            int n = sc.nextInt();
            int a1 = sc.nextInt();
            Node head = new Node(a1);
            addToTheLast(head);
            
            for(int i = 1; i< n; i++)
            {
                int a = sc.nextInt();
                addToTheLast(new Node(a));
            }
            
            GfG gfg = new GfG();
            Node node = gfg.quickSort(head);
            
            printList(node);
            System.out.println();
        }
    }
    public static void printList(Node head)
    {
        while(head != null)
        {
            System.out.print(head.data + " ");
            head = head.next;
        }
    }
    
    public static void addToTheLast(Node node) 
{
  if (head == null) 
  {
    head = node;
  } else 
  {
   Node temp = head;
   while (temp.next != null)
          temp = temp.next;
         temp.next = node;
  }
}
}
// } Driver Code Ends


/*node class of the linked list
class Node
{
    int data;
    Node next;
    Node(int key)
    {
        data = key;
        next = null;
    }
    
}*/
// you have to complete this function
class GfG {
    // This function takes the last element as pivot, places the pivot element
    // at its correct position in the sorted list, and places all smaller
    // (smaller than or equal to pivot) elements to the left of the pivot
    // and all greater elements to the right of the pivot.
    Node partition(Node start, Node end) {
        if (start == end || start == null || end == null)
            return start;

        Node pivot = end;
        Node tail = start;

        // Move 'curr' and swap smaller or equal elements than pivot on the left side.
        while (start != pivot) {
            if (start.data <= pivot.data) {
                int temp = start.data;
                start.data = tail.data;
                tail.data = temp;
                tail = tail.next;
            }
            start = start.next;
        }

        // Swap the pivot element with the element at tail.
        int temp = pivot.data;
        pivot.data = tail.data;
        tail.data = temp;

        return tail;
    }

    // Sorts the linked list using quicksort.
    void quickSortRecur(Node start, Node end) {
        if (start == null || end == null || start == end || start == end.next)
            return;
        
        Node pivot_prev = partition(start, end);
        if (pivot_prev != null && pivot_prev != start) {
            Node temp = start;
            while (temp.next != pivot_prev)
                temp = temp.next;
            quickSortRecur(start, temp);
        }
        quickSortRecur(pivot_prev.next, end);
    }

    // The main function to sort a linked list. It mainly calls quickSortRecur().
    public Node quickSort(Node node) {
        Node start = node;
        Node end = node;
        while (end.next != null)
            end = end.next;
        quickSortRecur(start, end);
        return start;
    }
}
